package Model;

import View.MainFrame;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Tosa Andrei
 * @brief the store simulator
 * @see Coada
 * @see Client
 * <p>
 * The Store class has the following properties: a private integer which
 * represents the number of queues, an array list of queues, to store all
 * the queues in the program, an array list of clients, to store the clients
 * generated for the simulation, in order of arrival time, a logger which is
 * used to log the most important events of the application and an instance
 * of the frame which was a last resort for displaying the information.
 */

public class Store extends Thread {
    private int nrOfQueues;
    private ArrayList<Coada> queues;
    private ArrayList<Client> clients;
    private Logger logger = Logger.getLogger("logi");
    private MainFrame frame;

    public Store(int nrOfQueues, MainFrame frame) {
        this.frame = frame;
        this.nrOfQueues = nrOfQueues;
        queues = new ArrayList<Coada>();
        clients = new ArrayList<Client>();

        for (int i = 1; i <= nrOfQueues; i++) {
            Coada aux = new Coada(i);
            queues.add(aux);
        }
    }

    public void startThreads() {
        for (Coada q : queues) {
            q.start();
        }
    }

    public void stopThreads() {
        for (Coada q : queues) {
            q.stopRunning();
        }
    }

    public int addClient(Client c) {
        Coada aux = queues.get(0);
        for (Coada q : queues) {
            q.waitTime += q.getWaitingTime();
            if (q.getWaitingTime() < aux.getWaitingTime())
                aux = q;
        }

        aux.add(c);
        aux.totalClients++;
        aux.serviceTime += c.getServiceTime();
        return aux.getWaitingTime() - c.getServiceTime();
    }

    /**
     * @brief Generates clients
     *
     *    The generate clients function takes the input simulation
     *    parameters and uses them to randomly generate a list of
     *    clients which are later used for the simulation.
     */
    public void generateClients(int interval, int minArrive, int maxArrive, int minService, int maxService) {
        logger.info("Generating clients");

        int t = 1;
        int id = 1;
        int ra = 0;
        int rs = 0;
        while (t + ra < interval) {
            rs = minService + ((int) (Math.random() * (maxService - minService + 1)));
            if (t + rs <= interval) clients.add(new Client(id++, t, rs));

            ra = minArrive + ((int) (Math.random() * (maxArrive - minArrive + 1)));
            t += ra;
        }
    }

    /**
     * @brief Simulator for the store
     *
     * An important function is the simulate function, which takes
     * care of the whole process of simulation. It generates the
     * clients, starts the threads and adds them when they arrive.
     * The information received here is sent to the controller for
     * it to be able to display it on the user interface.
     */
    public void simulate(int interval, int minArrive, int maxArrive, int minService, int maxService, MainFrame frame) {
        generateClients(interval, minArrive, maxArrive, minService, maxService);
        frame.isRunning = true;
        startThreads();
        int t = 1;
        int clientNr = 0;
        while (t <= interval) {
            frame.setTimeLabel(t);
            logger.log(Level.INFO, "TIME " + t);

            if (clientNr < clients.size() && clients.get(clientNr).getArriveTime() == t) {
                addClient(clients.get(clientNr));
                clientNr++;
            }
            frame.setQueueLabel(toString());

            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            t++;
        }

        stopThreads();
        frame.setQueueLabel(getStatistics());
        frame.isRunning = false;
    }

    public void run() {
        simulate(frame.getSimIntervalVal(), frame.getMinArriveVal(), frame.getMaxArriveVal(), frame.getMinServiceVal(), frame.getMaxServiceVal(), frame);
    }

    public String toString() {
        String s = "<html> <br>";
        int index = 1;
        for (Coada q : queues) {
            s += "C" + index++ + ": " + q.toString() + "<br>";
        }
        s += "</html>";

        return s;
    }

    public String getStatistics() {
        int empty = 0;
        double avgWait = 0;
        int service = 0;

        String s = "<html> <br>";
        int index = 1;
        for (Coada q : queues) {
            s += "C" + index++ + ": ";
            s += "Empty( " + q.emptyTime + " )  ";
            empty += q.emptyTime;
            s += "AvgWait( " + q.waitTime / q.totalClients + " )  ";
            avgWait += q.waitTime / q.totalClients;
            s += "Service( " + q.serviceTime + " )";
            service += q.serviceTime;
            s += "<br>";
        }

        s += "<br>Total<br>";
        s += "Empty: " + empty + "  avg: " + empty / nrOfQueues + "<br>";
        s += "Wait: " + avgWait / nrOfQueues + "<br>";
        s += "Service: " + service + "  avg: " + service / nrOfQueues + "<br>";

        s += "</html>";

        return s;
    }
}
