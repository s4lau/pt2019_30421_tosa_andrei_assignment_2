package Model;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * @author Tosa Andrei
 * @brief the queue simulator
 * @see Store
 * @see Client
 * <p>
 * The attributes of the queue class are a queue based on a linked list of clients,
 * which stores the clients, in the order that they arrived. This is used to simulate
 * the moving of the queue. Also, the id of the queue is stored, just for logging and
 * a more easy display of the simulation. In addition, some integers are maintained
 * to compute the statistics regarding the simulation. They remember the total number
 * of customers, the average waiting time, the sum of the service time for each
 * customer, and the time the queue was empty.
 */
public class Coada extends Thread {
    private int id;
    private Queue<Client> coada;
    private boolean running;
    private Logger logger = Logger.getLogger("logi");
    public int emptyTime = 0;
    public double waitTime = 0;
    public int serviceTime = 0;
    public int totalClients = 0;

    public Coada(int id) {
        this.id = id;
        coada = new LinkedList<>();
    }

    public int getWaitingTime() {
        int t = 0;
        for (Client c : coada)
            t += c.getServiceTime();

        return t;
    }

    public boolean add(Client c) {
        return coada.add(c);
    }

    public void stopRunning() {
        running = false;
    }

    public void run() {
        running = true;

        try {
            TimeUnit.MILLISECONDS.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //System.out.println("Queue " + this.id + " started");
        logger.log(Level.INFO, "Queue " + this.id + " started");
        while (running) {
            if (!coada.isEmpty()) {
                //System.out.println(" " + this.id + ": " + coada.peek().toString());
                logger.log(Level.INFO, " " + this.id + ": " + coada.peek().toString());
                if (coada.peek().decreaseTime() <= 0) {
                    //System.out.println(this.id + ": removed " + coada.peek().toString());
                    coada.poll();
                }
            } else emptyTime++;

            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public String toString() {
        String s = "";
        for (Client c : coada) {
            s += c.toString() + " ";
        }

        return s;
    }
}
