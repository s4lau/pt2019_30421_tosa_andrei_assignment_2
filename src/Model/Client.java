package Model;
/**
 * @author Tosa Andrei
 * @brief the client information
 * <p>
 * The attributes of the Client class are a private
 * int to store the id, a private int to store the
 * arrival time and a private int to store the service
 * time needed for the customers task.
 */
public class Client {
    private int id;
    private int arriveTime;
    private int serviceTime;

    public Client(int id, int arriveTime, int serviceTime) {
        this.id = id;
        this.arriveTime = arriveTime;
        this.serviceTime = serviceTime;
    }

    public int getId() {
        return id;
    }

    public int getArriveTime() {
        return arriveTime;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public int decreaseTime() {
        return --serviceTime;
    }

    public String toString() {
        return "C" + this.id + "[ " + this.serviceTime + " (" + this.arriveTime + ") ]";
    }
}
