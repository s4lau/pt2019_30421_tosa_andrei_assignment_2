package View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * @author Tosa Andrei
 * @brief The main frame of the application
 * @see Controller
 * <p>
 * The View represents the graphical user interface. It is made
 * up of J components, which don’t do anything by themselves, so
 * the whole functionality of the program is implemented in the Controller.
 */

public class MainFrame extends JFrame {
    private GridBagConstraints gbc = new GridBagConstraints();
    private JButton decMinArrive, incMinArrive, decMaxArrive, incMaxArrive;
    private JButton decMinService, incMinService, decMaxService, incMaxService;
    private JButton decQueues, incQueues, decInterval, incInterval;
    private JButton startButton;
    private JLabel minArrive, maxArrive, minService, maxService, nrOfQueues, simInterval;
    private JLabel timeLabel, queueLabel;

    private int minArriveVal = 1;
    private int maxArriveVal = 1;
    private int minServiceVal = 1;
    private int maxServiceVal = 1;
    private int nrOfQueuesVal = 1;
    private int simIntervalVal = 30;

    public boolean isRunning = false;

    public MainFrame(String title) {
        super(title);
        setSize(500, 800);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        setLayout(new GridBagLayout());
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets = new Insets(3, 3, 3, 3);

        startButton = new JButton("START");
        startButton.setActionCommand("start");
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        add(startButton, gbc);

        timeLabel = new JLabel("");
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        add(timeLabel, gbc);

        queueLabel = new JLabel("");
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.gridwidth = 6;
        add(queueLabel, gbc);

        setArrival();
        setService();
        setQueues();
        setInterval();
    }

    private void setArrival() {
        /** Arrival time interval */
        JLabel atLabel = new JLabel("Arrival time interval:");
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 1;
        add(atLabel, gbc);

        decMinArrive = new JButton("-");
        decMinArrive.setActionCommand("decMinArrive");
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.gridwidth = 1;
        add(decMinArrive, gbc);

        minArrive = new JLabel("1");
        gbc.gridx = 2;
        gbc.gridy = 1;
        gbc.gridwidth = 1;
        add(minArrive, gbc);

        incMinArrive = new JButton("+");
        incMinArrive.setActionCommand("incMinArrive");
        gbc.gridx = 3;
        gbc.gridy = 1;
        gbc.gridwidth = 1;
        add(incMinArrive, gbc);

        decMaxArrive = new JButton("-");
        decMaxArrive.setActionCommand("decMaxArrive");
        gbc.gridx = 4;
        gbc.gridy = 1;
        gbc.gridwidth = 1;
        add(decMaxArrive, gbc);

        maxArrive = new JLabel("1");
        gbc.gridx = 5;
        gbc.gridy = 1;
        gbc.gridwidth = 1;
        add(maxArrive, gbc);

        incMaxArrive = new JButton("+");
        incMaxArrive.setActionCommand("incMaxArrive");
        gbc.gridx = 6;
        gbc.gridy = 1;
        gbc.gridwidth = 1;
        add(incMaxArrive, gbc);
    }

    private void setService() {
        /** Service time */
        JLabel stLabel = new JLabel("Service time:");
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        add(stLabel, gbc);

        decMinService = new JButton("-");
        decMinService.setActionCommand("decMinService");
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        add(decMinService, gbc);

        minService = new JLabel("1");
        gbc.gridx = 2;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        add(minService, gbc);

        incMinService = new JButton("+");
        incMinService.setActionCommand("incMinService");
        gbc.gridx = 3;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        add(incMinService, gbc);

        decMaxService = new JButton("-");
        decMaxService.setActionCommand("decMaxService");
        gbc.gridx = 4;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        add(decMaxService, gbc);

        maxService = new JLabel("1");
        gbc.gridx = 5;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        add(maxService, gbc);

        incMaxService = new JButton("+");
        incMaxService.setActionCommand("incMaxService");
        gbc.gridx = 6;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        add(incMaxService, gbc);
    }

    private void setQueues() {
        JLabel qLabel = new JLabel("Number of Queues:");
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        add(qLabel, gbc);

        decQueues = new JButton("-");
        decQueues.setActionCommand("decQueues");
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        add(decQueues, gbc);

        nrOfQueues = new JLabel("1");
        gbc.gridx = 2;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        add(nrOfQueues, gbc);

        incQueues = new JButton("+");
        incQueues.setActionCommand("incQueues");
        gbc.gridx = 3;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        add(incQueues, gbc);
    }

    private void setInterval() {
        JLabel iLabel = new JLabel("Simulation interval:");
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.gridwidth = 1;
        add(iLabel, gbc);

        decInterval = new JButton("-");
        decInterval.setActionCommand("decInterval");
        gbc.gridx = 1;
        gbc.gridy = 4;
        gbc.gridwidth = 1;
        add(decInterval, gbc);

        simInterval = new JLabel("30");
        gbc.gridx = 2;
        gbc.gridy = 4;
        gbc.gridwidth = 1;
        add(simInterval, gbc);

        incInterval = new JButton("+");
        incInterval.setActionCommand("incInterval");
        gbc.gridx = 3;
        gbc.gridy = 4;
        gbc.gridwidth = 1;
        add(incInterval, gbc);
    }

    public void setActionListener(ActionListener a) {
        decMinArrive.addActionListener(a);
        incMinArrive.addActionListener(a);
        decMaxArrive.addActionListener(a);
        incMaxArrive.addActionListener(a);


        decMinService.addActionListener(a);
        incMinService.addActionListener(a);
        decMaxService.addActionListener(a);
        incMaxService.addActionListener(a);

        decQueues.addActionListener(a);
        incQueues.addActionListener(a);

        decInterval.addActionListener(a);
        incInterval.addActionListener(a);

        startButton.addActionListener(a);
    }

    public void setMinArriveVal(int x) {
        if (minArriveVal + x >= 1 && minArriveVal + x <= maxArriveVal) minArriveVal += x;
        minArrive.setText("" + minArriveVal);
    }

    public int getMinArriveVal() {
        return minArriveVal;
    }

    public void setMaxArriveVal(int x) {
        if (maxArriveVal + x >= 1 && maxArriveVal + x >= minArriveVal) maxArriveVal += x;
        maxArrive.setText("" + maxArriveVal);
    }

    public int getMaxArriveVal() {
        return maxArriveVal;
    }

    public void setMinServiceVal(int x) {
        if (minServiceVal + x >= 1 && minServiceVal + x <= maxServiceVal) minServiceVal += x;
        minService.setText("" + minServiceVal);
    }

    public int getMinServiceVal() {
        return minServiceVal;
    }

    public void setMaxServiceVal(int x) {
        if (maxServiceVal + x >= 1 && maxServiceVal + x >= minServiceVal) maxServiceVal += x;
        maxService.setText("" + maxServiceVal);
    }

    public int getMaxServiceVal() {
        return maxServiceVal;
    }

    public void setNrOfQueuesVal(int x) {
        if (nrOfQueuesVal + x >= 1 && nrOfQueuesVal + x <= 5) nrOfQueuesVal += x;
        nrOfQueues.setText("" + nrOfQueuesVal);
    }

    public int getNrOfQueuesVal() {
        return nrOfQueuesVal;
    }

    public void setSimIntervalVal(int x) {
        if (simIntervalVal + x >= 1 && simIntervalVal + x <= 60) simIntervalVal += x;
        simInterval.setText("" + simIntervalVal);
    }

    public int getSimIntervalVal() {
        return simIntervalVal;
    }

    public void setTimeLabel(int t) {
        timeLabel.setText("" + t);
    }

    public void setQueueLabel(String s) {
        queueLabel.setText(s);
    }
}
