package Controller;

import Model.Store;
import View.MainFrame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * @author Tosa Andrei
 * @brief the controller of the application
 * @see MainFrame
 * <p>
 * The Controller package unites the Model package with the View package,
 * and basically gives the apps whole functionality. It handles the user
 * input and the operations computed in the model, and displays the results
 * on the graphical interface.
 */

public class Controller {

    public static void main(String args[]) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainFrame frame = new MainFrame("Queue simulator");

                Logger logger = Logger.getLogger("logi");
                FileHandler fh;
                try {
                    fh = new FileHandler("C:/Users/Andy/Documents/Java/MyLogFile.log");
                    logger.addHandler(fh);
                    SimpleFormatter formatter = new SimpleFormatter();
                    fh.setFormatter(formatter);
                } catch (SecurityException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                logger.log(Level.INFO, "Program started");

                frame.setActionListener(new ActionListener() {
                    @Override
                    /**
                     * @brief Action listener for the main frame
                     *
                     * The function is assigned as an action listener to all the buttons in
                     * the main frame view. When an event takes place, we can determine the
                     * button that was pressed by the action command assigned to it.
                     */
                    public void actionPerformed(ActionEvent e) {
                        String action = e.getActionCommand();

                        logger.log(Level.INFO, "Action: " + action);

                        if (action.equals("start")) {
                            if (frame.isRunning == false) {
                                logger.log(Level.INFO, "Starting simulation");

                                Store s = new Store(frame.getNrOfQueuesVal(), frame);
                                s.start();

                                logger.log(Level.INFO, "Finished simulation");
                            }
                        }

                        if (action.equals("decMinArrive"))
                            frame.setMinArriveVal(-1);
                        if (action.equals("incMinArrive"))
                            frame.setMinArriveVal(1);
                        if (action.equals("decMaxArrive"))
                            frame.setMaxArriveVal(-1);
                        if (action.equals("incMaxArrive"))
                            frame.setMaxArriveVal(1);

                        if (action.equals("decMinService"))
                            frame.setMinServiceVal(-1);
                        if (action.equals("incMinService"))
                            frame.setMinServiceVal(1);
                        if (action.equals("decMaxService"))
                            frame.setMaxServiceVal(-1);
                        if (action.equals("incMaxService"))
                            frame.setMaxServiceVal(1);

                        if (action.equals("decQueues"))
                            frame.setNrOfQueuesVal(-1);
                        if (action.equals("incQueues"))
                            frame.setNrOfQueuesVal(1);

                        if (action.equals("decInterval"))
                            frame.setSimIntervalVal(-1);
                        if (action.equals("incInterval"))
                            frame.setSimIntervalVal(1);
                    }
                });
            }
        });


    }
}
